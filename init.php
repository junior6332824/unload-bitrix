<?
//Отменяем изменения привязки у существующих товаров к разделам во время обмена с 1С

AddEventHandler("iblock", "OnBeforeIBlockElementUpdate","DoNotUpdate");
function DoNotUpdate(&$arFields)
{
    if ($_REQUEST['mode']=='import')
    {
        unset($arFields['IBLOCK_ELEMENT_SECTION_ID']); 
		unset($arFields['IBLOCK_SECTION']); 
    }
}
?>